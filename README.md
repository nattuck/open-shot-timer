
# Open Shot Timer

Can this be updated to run on modern Android? We'll see.

## Google Code Description

from: https://code.google.com/archive/p/openshottimer/

GPL Shot Timer for the Android platform

A GPLed Shot Timer written in Java targeting the Android platform. The goal of
the project is to provide a shot timer with functionality on par with commodity
timers like the CED 7k/8k along with new features that exploit the camera and
connectivity that most Android devices have.

## Initial Author

App was found on this forum thread:
https://forums.brianenos.com/topic/86330-open-source-shot-timer/

Poster's name: adweisbe

## License

GNU GPLv3
